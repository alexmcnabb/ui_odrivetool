# Troubleshooting

Testing on windows, I had an import error on `from odrive import fibre`, solved by adding `import fibre` to `C:\Users\alex\AppData\Local\Programs\Python\Python310\lib\site-packages\odrive\__init__.py` after `sys.path.insert`

Got `Could not open USB device: -5`, cleared by installing a WinUSB drive with [Zadig](https://zadig.akeo.ie/)

To install requirements: `pip install -r requirements.txt`
