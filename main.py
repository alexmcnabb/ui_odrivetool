#!/usr/bin/env python3

from cmath import inf
import math
import sys, os
from time import time

import pyqtgraph as pg
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from functools import partial, wraps
import traceback
import signal

import odrive.enums as ode
from odrive import fibre

from serialThread import odriveWorker
from parameter import Parameter, ParamTypes

os.environ["DISPLAY"] = ":0"  # Force display to run locally, even if launched over ssh

# Good info here about changes between versions
# https://github.com/odriverobotics/ODrive/blob/master/CHANGELOG.md

# Odrive Reference
# https://docs.odriverobotics.com/v/0.5.4/fibre_types/com_odriverobotics_ODrive.html

class FilteredDataReadout:
	def __init__(self, textbox, data_source, axis_specific=True, func=lambda x: x, format_string="{:9.2f}", alpha=0):
		self.textbox = textbox
		self.data_source = data_source
		self.axis_specific = axis_specific
		self.format_string = format_string
		self.alpha = alpha
		self.value = 0
		self.func = func

	def update(self, odrv_object, active_axis):
		ob = active_axis if self.axis_specific else odrv_object
		for objname in self.data_source.split('.'):
			ob = getattr(ob, objname)
		self.value = self.value * self.alpha + float(ob) * (1-self.alpha)
		s = self.format_string.format(self.func(self.value))
		self.textbox.setText(s)


def disconnect_on_error(func):
	"""A Decorator to cleanly stop querying stuff if the odrive disconnects.
	Can't go inside the class if we want to use it as a decorator"""
	@wraps(func)
	def wrapper(self, *args, **kwargs):
		try:
			func(self, *args, **kwargs)
		except (AttributeError):#, ChannelBrokenException, ChannelDamagedException):
			traceback.print_exc()  # Not having this line hides lots of exceptions, so you might want it for debugging
			self.handle_disconnect()
	return wrapper


class PlottedCurve:
	def __init__(self, widget, name, pen, data_source, label_parts=None):
		self.plotwidget = widget
		self.name = name
		self.pen = pen
		self.data_source = data_source
		self.starttime = self.times = self.datas = None
		self.label_parts = label_parts
		self.reset()

	def reset(self):
		if self.label_parts is not None:
			self.plotwidget.clear()
			self.plotwidget.setLabel(*self.label_parts[0])
			self.plotwidget.setLabel(*self.label_parts[1])
			self.plotwidget.setTitle(*self.label_parts[2])
		self.curve = self.plotwidget.plot(name=self.name, pen=self.pen)
		self.times = []
		self.datas = []
		self.starttime = time()
		self.curve.setData(self.times, self.datas)
	
	def add_data(self, axis, maxlength):
		data = self.data_source(axis)
		self.datas.append(data)
		self.times.append(time() - self.starttime)
		self.curve.setData(self.times, self.datas)
		c = 0
		while self.times[-1] - self.times[0] > maxlength:
			c += 1
			self.times.pop(0)
			self.datas.pop(0)


class MainWindow(QtWidgets.QMainWindow):
	def __init__(self):
		super(self.__class__, self).__init__()
		uic.loadUi('mainwindow.ui', self)
		self.my_drive = None
		self.last_state = None
		self.timer = None
		self.timer_graphUpdate = None

		signal.signal(signal.SIGINT, signal.SIG_DFL)  # Allow Ctrl+C in the command window to kill the program

		# Header buttons
		self.pushButton_saveConfiguration.clicked.connect(lambda: self.my_drive.save_configuration())
		self.pushButton_eraseConfiguration.clicked.connect(self.odrive_erase)
		self.pushButton_scanConfiguration.clicked.connect(self.scan_all_config)
		self.pushButton_writeConfiguration.clicked.connect(self.write_config)
		self.pushButton_connect.clicked.connect(self.odrive_connect)
		self.pushButton_reboot.clicked.connect(self.odrive_reboot)
		self.comboBoxActiveAxis.currentIndexChanged.connect(self.active_axis_changed)
		
		# Axis motion controls
		self.buttonGroup_13.buttonClicked.connect(self.controller_mode_changed)
		self.axis0PositionGo_pushButton.clicked.connect(self.send_axis_position_go)
		self.axis0Backward_pushButton.clicked.connect(self.send_axis_velocity_current_backward)
		self.axis0Stop_pushButton.clicked.connect(self.send_axis_velocity_current_stop)
		self.axis0Forward_pushButton.clicked.connect(self.send_axis_velocity_current_forward)
		self.clearGraph_pushButton.clicked.connect(self.reset_graphs)
		self.pushButton_clearErrors.clicked.connect(self.clear_errors)

		# State machine interface
		self.all_state_buttons = {
			ode.AXIS_STATE_IDLE: self.axis0_pushButton_idle,
			ode.AXIS_STATE_STARTUP_SEQUENCE: self.axis0_pushButton_startupSequence,
			ode.AXIS_STATE_FULL_CALIBRATION_SEQUENCE: self.axis0_pushButton_fullCalibrationSequence,
			ode.AXIS_STATE_MOTOR_CALIBRATION: self.axis0_pushButton_motorCalibration,
			# ode.AXIS_STATE_SENSORLESS_CONTROL: self.axis0_pushButton_sensorlessControl,  // Replaced with <odrv>.enable_sensorless_mode = True
			ode.AXIS_STATE_ENCODER_INDEX_SEARCH: self.axis0_pushButton_encoderIndexSearch,
			ode.AXIS_STATE_ENCODER_OFFSET_CALIBRATION: self.axis0_pushButton_encoderOffsetCalibration,
			ode.AXIS_STATE_CLOSED_LOOP_CONTROL: self.axis0_pushButton_closedLoopControl,
		}
		def machine_state_clicked(self, state):
			self.active_axis.requested_state = state
		for state, state_button in self.all_state_buttons.items():
			state_button.clicked.connect(partial(machine_state_clicked, self, state))

		# Config options
		self.all_config_parameters = []
		system_stats = [
			Parameter("HW Version Major", "hw_version_major", ParamTypes.ReadonlyString, False),
			Parameter("HW Version Minor", "hw_version_minor", ParamTypes.ReadonlyString, False),
			Parameter("HW Version Variant", "hw_version_variant", ParamTypes.ReadonlyString, False),
			Parameter("FW Version Major", "fw_version_major", ParamTypes.ReadonlyString, False),
			Parameter("FW Version Minor", "fw_version_minor", ParamTypes.ReadonlyString, False),
			Parameter("FW Version Revision", "fw_version_revision", ParamTypes.ReadonlyString, False),
			Parameter("FW Version Unreleased", "fw_version_unreleased", ParamTypes.ReadonlyString, False),
			Parameter("User Config Loaded", "user_config_loaded", ParamTypes.ReadonlyString, False),
			Parameter("Brake Resistor Armed", "brake_resistor_armed", ParamTypes.ReadonlyString, False),
			Parameter("Brake Resistor Current", "brake_resistor_current", ParamTypes.ReadonlyString, False, format_func=lambda x: f"{x:.2f}A"),
			Parameter("Brake Resistor Saturated", "brake_resistor_saturated", ParamTypes.ReadonlyString, False),
			Parameter("Serial Number", "serial_number", ParamTypes.ReadonlyString, False),
			Parameter("VBus Voltage", "vbus_voltage", ParamTypes.ReadonlyString, False, format_func=lambda x: f"{x:.2f}V"),
			Parameter("IBus Current", "ibus", ParamTypes.ReadonlyString, False, format_func=lambda x: f"{x:.2f}A"),
			Parameter("Misconfigured", "misconfigured", ParamTypes.ReadonlyString, False),
			Parameter("I2C Address", "system_stats.i2c.addr", ParamTypes.ReadonlyString, False),
			Parameter("I2C Addr Match Count", "system_stats.i2c.addr_match_cnt", ParamTypes.ReadonlyString, False),
			Parameter("I2C Error Count", "system_stats.i2c.error_cnt", ParamTypes.ReadonlyString, False),
			Parameter("I2C RX Count", "system_stats.i2c.rx_cnt", ParamTypes.ReadonlyString, False),
			Parameter("Min Heap Space", "system_stats.min_heap_space", ParamTypes.ReadonlyString, False),
			Parameter("Max Stack Usage Axis", "system_stats.max_stack_usage_axis", ParamTypes.ReadonlyString, False),
			Parameter("Min Stack Usage CAN", "system_stats.max_stack_usage_can", ParamTypes.ReadonlyString, False),
			Parameter("Min Stack Usage Startup", "system_stats.max_stack_usage_startup", ParamTypes.ReadonlyString, False),
			Parameter("Min Stack Usage UART", "system_stats.max_stack_usage_uart", ParamTypes.ReadonlyString, False),
			Parameter("Min Stack Usage USB", "system_stats.max_stack_usage_usb", ParamTypes.ReadonlyString, False),
			Parameter("Stack Size Analog", "system_stats.stack_size_analog", ParamTypes.ReadonlyString, False),
			Parameter("Stack Size Axis", "system_stats.stack_size_axis", ParamTypes.ReadonlyString, False),
			Parameter("Stack Size CAN", "system_stats.stack_size_can", ParamTypes.ReadonlyString, False),
			Parameter("Stack Size Startup", "system_stats.stack_size_startup", ParamTypes.ReadonlyString, False),
			Parameter("Stack Size UART", "system_stats.stack_size_uart", ParamTypes.ReadonlyString, False),
			Parameter("Stack Size USB", "system_stats.stack_size_usb", ParamTypes.ReadonlyString, False),
			Parameter("Uptime", "system_stats.uptime", ParamTypes.ReadonlyString, False),
			Parameter("USB RX Count", "system_stats.usb.rx_cnt", ParamTypes.ReadonlyString, False),
			Parameter("USB TX Count", "system_stats.usb.tx_cnt", ParamTypes.ReadonlyString, False),
			Parameter("USB TX Overrun Count", "system_stats.usb.tx_overrun_cnt", ParamTypes.ReadonlyString, False),
		]
		self.fill_groupbox(self.groupBox_system_stats, system_stats)
		self.all_config_parameters.extend(system_stats)
		system_config = [
			Parameter("DC Bus Overvoltage Ramp End", "config.dc_bus_overvoltage_ramp_end", ParamTypes.Float, False),
			Parameter("DC Bus Overvoltage Ramp Start", "config.dc_bus_overvoltage_ramp_start", ParamTypes.Float, False),
			Parameter("DC Bus Overvoltage Trip Level", "config.dc_bus_overvoltage_trip_level", ParamTypes.Float, False),
			Parameter("DC Bus Undervoltage Trip Level", "config.dc_bus_undervoltage_trip_level", ParamTypes.Float, False),
			Parameter("DC Max Negative Current", "config.dc_max_negative_current", ParamTypes.Float, False),
			Parameter("DC Max Positive Current", "config.dc_max_positive_current", ParamTypes.Float, False),
			Parameter("Enable Brake Resistor", "config.enable_brake_resistor", ParamTypes.Bool, False),
			Parameter("Brake Resistance", "config.brake_resistance", ParamTypes.Float, False),
			Parameter("Enable CAN A", "config.enable_can_a", ParamTypes.Bool, False),
			Parameter("Enable DC Bus Overvoltage Ramp", "config.enable_dc_bus_overvoltage_ramp", ParamTypes.Bool, False),
			Parameter("Enable I2C A", "config.enable_i2c_a", ParamTypes.Bool, False),
			Parameter("Enable UART A", "config.enable_uart_a", ParamTypes.Bool, False),
			Parameter("Enable UART B", "config.enable_uart_b", ParamTypes.Bool, False),
			Parameter("Enable UART C", "config.enable_uart_c", ParamTypes.Bool, False),
			Parameter("Max Regen Current", "config.max_regen_current", ParamTypes.Float, False),
			Parameter("UART A Protocol", "config.uart0_protocol", ParamTypes.Int, False),
			Parameter("UART B Protocol", "config.uart1_protocol", ParamTypes.Int, False),
			Parameter("UART C Protocol", "config.uart2_protocol", ParamTypes.Int, False),
			Parameter("UART A Baudrate", "config.uart_a_baudrate", ParamTypes.Int, False),
			Parameter("UART B Baudrate", "config.uart_b_baudrate", ParamTypes.Int, False),
			Parameter("UART C Baudrate", "config.uart_c_baudrate", ParamTypes.Int, False),
			Parameter("USB CDC Protocol", "config.usb_cdc_protocol", ParamTypes.Int, False),
		]
		self.fill_groupbox(self.groupBox_system_config, system_config)
		self.all_config_parameters.extend(system_config)
		axis_config = [
			Parameter("Driver Fault Code", "last_drv_fault", ParamTypes.ReadonlyString, True, format_func=lambda x: f"S1: 0x{(x & 0xFFFF):04X} S2: 0x{((x >> 16) & 0xFFFF):04X}"), # TODO: Doesn't this cover both axes?
			Parameter("Enable Motor Cal on Startup", "config.startup_motor_calibration", ParamTypes.Bool, True),
			Parameter("Enable Index search on Startup", "config.startup_encoder_index_search", ParamTypes.Bool, True),
			Parameter("Enable Encoder Offset Cal on Startup", "config.startup_encoder_offset_calibration", ParamTypes.Bool, True),
			Parameter("Enable Closed Loop Control on Startup", "config.startup_closed_loop_control", ParamTypes.Bool, True),
			Parameter("Enable Step Direction Input", "config.enable_step_dir", ParamTypes.Bool, True),
			# Parameter("", "", ParamTypes., True),
		]
		self.fill_groupbox(self.groupBox_axis_config, axis_config)
		self.all_config_parameters.extend(axis_config)
		encoder_config = [
			Parameter("Bandwidth", "encoder.config.bandwidth", ParamTypes.Float, True),
			Parameter("Counts per Rev", "encoder.config.cpr", ParamTypes.Int, True),
			Parameter("Use Index", "encoder.config.use_index", ParamTypes.Bool, True),
			Parameter("Index Offset", "encoder.config.index_offset", ParamTypes.Float, True),
			Parameter("Precalibrated", "encoder.config.pre_calibrated", ParamTypes.Bool, True),
			Parameter("Counts per Rev", "encoder.config.cpr", ParamTypes.Int, True),
			Parameter("Bandwidth", "encoder.config.bandwidth", ParamTypes.Float, True),
			Parameter("Index found", "encoder.index_found", ParamTypes.ReadonlyString, True),
			Parameter("Is Ready", "encoder.is_ready", ParamTypes.ReadonlyString, True),
			Parameter("Position Estimate", "encoder.pos_estimate", ParamTypes.ReadonlyString, True),
			Parameter("Shadow Count", "encoder.shadow_count", ParamTypes.ReadonlyString, True),
			Parameter("Interpolation", "encoder.interpolation", ParamTypes.ReadonlyString, True), 
			Parameter("Count in CPR", "encoder.count_in_cpr", ParamTypes.ReadonlyString, True), 
			Parameter("Hall State", "encoder.hall_state", ParamTypes.ReadonlyString, True), 
			Parameter("Phase", "encoder.phase", ParamTypes.ReadonlyString, True), 
		]
		self.fill_groupbox(self.groupBox_encoder_config, encoder_config)
		self.all_config_parameters.extend(encoder_config)
		controller_config = [
			Parameter("Velocity Limit", "controller.config.vel_limit", ParamTypes.Float, True),
			Parameter("Velocity Integrator gain", "controller.config.vel_integrator_gain", ParamTypes.Float, True),
			Parameter("Enable Velocity Limit", "controller.config.enable_vel_limit", ParamTypes.Bool, True),
			Parameter("Enable Overspeed Error", "controller.config.enable_overspeed_error", ParamTypes.Bool, True),
			# Parameter("", "controller.config.", ParamTypes., True),
		]
		self.fill_groupbox(self.groupBox_controller_config, controller_config)
		self.all_config_parameters.extend(controller_config)
		motor_config = [
			Parameter("Is Calibrated", "motor.is_calibrated", ParamTypes.ReadonlyString, True),
			Parameter("Pre-Calibrated", "motor.config.pre_calibrated", ParamTypes.Bool, True),
			Parameter("Phase Inductance", "motor.config.phase_inductance", ParamTypes.ReadonlyString, True),
			Parameter("Phase Resistance", "motor.config.phase_resistance", ParamTypes.ReadonlyString, True),
			Parameter("Current Control Bandwidth", "motor.config.current_control_bandwidth", ParamTypes.Float, True),
			Parameter("Current Limit", "motor.config.current_lim", ParamTypes.Float, True),
			Parameter("Requested Current Range", "motor.config.requested_current_range", ParamTypes.Float, True),
			Parameter("Pole Pairs", "motor.config.pole_pairs", ParamTypes.Int, True),
			Parameter("Calibration Current", "motor.config.calibration_current", ParamTypes.Float, True),
			# Parameter("", "motor.config.", ParamTypes., True),
		]
		self.fill_groupbox(self.groupBox_motor_config, motor_config)
		self.all_config_parameters.extend(motor_config)

		# Plot configuration
		redpen, yellowpen = pg.mkPen(color=(255, 78, 0), width=1), pg.mkPen(color=(255, 212, 0), width=1)
		self.curves = [
			PlottedCurve(self.plotWidget_velocity, "Setpoint", redpen, data_source=lambda axis: axis.controller.vel_setpoint, label_parts = [['bottom', 'Time', 's'], ['left', 'Velocity', 'rpm'], ["Velocity"]]),
			PlottedCurve(self.plotWidget_velocity, "Estimate", yellowpen, data_source=lambda axis: axis.encoder.vel_estimate),
			PlottedCurve(self.plotWidget_position, "Setpoint", redpen, data_source=lambda axis: axis.controller.pos_setpoint, label_parts = [['bottom', 'Time', 's'], ['left', 'Position', 'counts'], ["Position"]]),
			PlottedCurve(self.plotWidget_position, "Estimate", yellowpen, data_source=lambda axis: axis.encoder.pos_estimate),
			PlottedCurve(self.plotWidget_current, "Setpoint", redpen, data_source=lambda axis: axis.motor.current_control.Iq_setpoint, label_parts = [['bottom', 'Time', 's'], ['left', 'Current', 'Iq'], ["Current"]]),
			PlottedCurve(self.plotWidget_current, "Estimate", yellowpen, data_source=lambda axis: axis.motor.current_control.Iq_measured),
		]

		self.filtered_data_readouts = [
			FilteredDataReadout(self.lineEdit_state_current_actual, "motor.current_control.Iq_measured", alpha=0.95),
			FilteredDataReadout(self.lineEdit_state_current_limit, "motor.current_control.Iq_setpoint", alpha=0.95),
			FilteredDataReadout(self.lineEdit_state_velocity_actual, "encoder.vel_estimate", func=lambda x: x*60, alpha=0.9),
			FilteredDataReadout(self.lineEdit_state_velocity_limit, "controller.vel_setpoint", func=lambda x: x*60),
			FilteredDataReadout(self.lineEdit_state_position_actual, "encoder.pos_estimate", alpha=0.5),
			FilteredDataReadout(self.lineEdit_state_position_limit, "controller.pos_setpoint"),
			FilteredDataReadout(self.lineEdit_onboard_temp_sensor, "axis0.motor.fet_thermistor.temperature", axis_specific=False),
			FilteredDataReadout(self.lineEdit_onboard_temp_sensor_2, "axis1.motor.fet_thermistor.temperature", axis_specific=False),
		]

		self.timer = pg.QtCore.QTimer()
		self.timer.timeout.connect(self.update_statuses)
		self.timer_graphUpdate = pg.QtCore.QTimer()
		self.timer_graphUpdate.timeout.connect(self.update_graphs)

		self.set_ui_disabled(True)
		self.odrive_connect()

	@staticmethod
	def fill_groupbox(groupbox, params):
		layout = QtWidgets.QFormLayout()
		groupbox.setLayout(layout)
		for item in params:
			layout.addRow(item.label_widget, item.field_widget)

	# Active axis related stuff:
	@property
	def active_axis(self):
		return (self.my_drive.axis0, self.my_drive.axis1)[self.comboBoxActiveAxis.currentIndex()]

	def active_axis_changed(self):
		self.scan_all_config()
		self.reset_graphs()
		# TODO: Redundancy
		print(self.active_axis.controller.input_torque)
		self.doubleSpinBox_output_current.setValue(math.fabs(self.active_axis.controller.input_torque))
		self.axis0Velocity_doubleSpinBox.setValue(math.fabs(self.active_axis.controller.config.vel_limit))

	# Controller mode stuff
	def update_controller_mode(self):
		self.axis_controller_fields_position_enabled(self.active_axis.controller.config.control_mode)

	def controller_mode_changed(self, button_id=None): # Default to current when we call this manually
		if button_id:
			control_mode = {"Position": ode.CONTROL_MODE_POSITION_CONTROL,
							"Current": ode.CONTROL_MODE_TORQUE_CONTROL,
							"Velocity": ode.CONTROL_MODE_VELOCITY_CONTROL}[button_id.text()]
		else:
			control_mode = ode.CONTROL_MODE_TORQUE_CONTROL
		self.axis_controller_fields_position_enabled(control_mode)
		self.active_axis.controller.config.control_mode = control_mode

	def axis_controller_fields_position_enabled(self, control_mode):
		enabled = control_mode == ode.CONTROL_MODE_POSITION_CONTROL
		self.axis0Position_doubleSpinBox.setEnabled(enabled)
		self.axis0PositionGo_pushButton.setEnabled(enabled)
		self.axis0Backward_pushButton.setDisabled(enabled)
		self.axis0Forward_pushButton.setDisabled(enabled)
		self.axis0Stop_pushButton.setDisabled(enabled)
		self.doubleSpinBox_output_current.setDisabled(enabled)
		self.axis0Velocity_doubleSpinBox.setDisabled(enabled)

	def odrive_connect(self):
		self.odrive_worker = odriveWorker()
		self.odrive_worker.odrive_found_sig.connect(self.odrive_connected)
		self.odrive_worker.start()
		self.pushButton_connect.setDisabled(True)
		self.pushButton_connect.setText("Connecting . . .")

	def odrive_connected(self, my_drive):
		self.my_drive = my_drive
		self.pushButton_connect.setText("Connected")
		self.timer.start(500)
		self.starttime = time()
		self.timer_graphUpdate.start(20)
		self.set_ui_disabled(False)
		self.reset_graphs()
		# Use this to print out the entire api
		# print(self.my_drive._dump("", 99))
		# exit()
		self.scan_all_config()
		self.controller_mode_changed()
		# TODO: Redundancy
		self.doubleSpinBox_output_current.setValue(math.fabs(self.active_axis.controller.input_torque))
		self.axis0Velocity_doubleSpinBox.setValue(math.fabs(self.active_axis.controller.config.vel_limit))

	def odrive_reboot(self, _):
		self.handle_disconnect()
		try:
			self.my_drive.reboot()
		except fibre.libfibre.ObjectLostError:
			pass

	def odrive_erase(self):
		self.handle_disconnect()  # The odrive will reboot upon erasing
		try:
			self.my_drive.erase_configuration()
		except fibre.libfibre.ObjectLostError:
			pass

	def handle_disconnect(self):
		self.set_ui_disabled(True)
		if self.timer is not None:
			self.timer.stop()
			self.timer_graphUpdate.stop()
		self.pushButton_connect.setDisabled(False)
		self.pushButton_connect.setText("Connect")

	def set_ui_disabled(self, state):
		self.tabWidget.setDisabled(state)
		self.controlTab.setDisabled(state)
		self.groupBox_13.setDisabled(state)
		self.pushButton_reboot.setDisabled(state)
		self.pushButton_scanConfiguration.setDisabled(state)
		self.pushButton_writeConfiguration.setDisabled(state)
		self.pushButton_saveConfiguration.setDisabled(state)
		self.pushButton_eraseConfiguration.setDisabled(state)
		self.pushButton_clearErrors.setDisabled(state)
		self.comboBoxActiveAxis.setDisabled(state)

	@disconnect_on_error
	def update_graphs(self):
		for fdr in self.filtered_data_readouts:
			fdr.update(self.my_drive, self.active_axis)
		for curve in self.curves:
			curve.add_data(self.active_axis, maxlength=self.spinBox_graphTime.value())

	def reset_graphs(self):
		for curve in self.curves:
			curve.reset()

	@disconnect_on_error
	def update_statuses(self):
		# self.c_ll_bus_voltage.read(self.my_drive, self.active_axis)
		self.update_machine_state()
		self.update_controller_mode()
		self.update_error_status()

	def update_error_status(self):
		def get_error_strings(raw_error_code, possible_code_prefix):
			possible_codes = {code: value for code, value in ode.__dict__.items() if code.startswith(possible_code_prefix)}
			if raw_error_code == 0:
				return ["no error"]
			else:
				error_strings = []
				for codename, codeval in possible_codes.items():
					if raw_error_code & codeval != 0:
						error_strings.append(codename)
				return error_strings
		# TODO: There's some additional error catagories here not accounted for - SENSORLESS_ESTIMATOR_ERROR_, CAN_ERROR_
		axis_string = ",".join(get_error_strings(self.active_axis.error, "ODRIVE_ERROR_"))
		encoder_string = ",".join(get_error_strings(self.active_axis.encoder.error, "ENCODER_ERROR_"))
		controller_string = ",".join(get_error_strings(self.active_axis.controller.error, "CONTROLLER_ERROR_"))
		motor_string = ",".join(get_error_strings(self.active_axis.motor.error, "MOTOR_ERROR_"))
		odrive_string = ",".join(get_error_strings(self.my_drive.error, "ODRIVE_ERROR_"))
		error_string = f"Current axis errors: O: {odrive_string} A: {axis_string} E: {encoder_string}, C: {controller_string}, M: {motor_string}"
		self.statusBar.showMessage(error_string)

	def clear_errors(self):
		self.my_drive.error = ode.ODRIVE_ERROR_NONE
		self.active_axis.error = ode.AXIS_ERROR_NONE
		self.active_axis.motor.error = ode.MOTOR_ERROR_NONE
		# self.active_axis.encoder.error = ode.ENCODER_ERROR_NONE  # TODO: No longer exists?
		# self.active_axis.controller.error = ode.CONTROLLER_ERROR_NONE

	def update_machine_state(self):
		current_state = self.active_axis.current_state
		if self.last_state != current_state:
			self.last_state = current_state
			for state, state_buttons in self.all_state_buttons.items():
				style = "background-color: rgb(246, 224, 143);" if current_state == state else ""
				state_buttons.setStyleSheet(style)

	def write_config(self):
		for config_parameter in self.all_config_parameters:
			config_parameter.write(self.my_drive, self.active_axis)

	def scan_all_config(self):
		for config_parameter in self.all_config_parameters:
			config_parameter.read(self.my_drive, self.active_axis)

	def send_axis_position_go(self):
		self.active_axis.controller.input_pos = self.axis0Position_doubleSpinBox.value()

	# TODO: Replace with partials maybe?
	def send_axis_velocity_current_stop(self):
		self.set_motor_output_velocity_current(0)

	def send_axis_velocity_current_forward(self):
		self.set_motor_output_velocity_current(1)

	def send_axis_velocity_current_backward(self):
		self.set_motor_output_velocity_current(-1)

	def set_motor_output_velocity_current(self, direction):
		mode = self.active_axis.controller.config.control_mode # TODO: Make sure current/velocity checkboxes are updated right
		if mode == ode.CONTROL_MODE_TORQUE_CONTROL:
			self.active_axis.motor.config.torque_constant = 1
			self.active_axis.controller.config.vel_gain =  10
			self.active_axis.controller.input_torque = self.doubleSpinBox_output_current.value() * direction
			self.active_axis.controller.config.vel_limit = self.axis0Velocity_doubleSpinBox.value()
		elif mode == ode.CONTROL_MODE_VELOCITY_CONTROL:
			pass
			# self.active_axis.controller.input_vel = value
		

	# def send_axis_velocity_current_command(self, mode, value):
	# 	if mode == ode.CONTROL_MODE_TORQUE_CONTROL:
	# 		# TODO: temp config settings shouldn't be here
	# 		self.active_axis.motor.config.torque_constant = 1
	# 		# print(self.active_axis.controller.config.vel_gain)
	# 		self.active_axis.controller.config.vel_gain =  2
	# 		# self.active_axis.config.I_bus_hard_min = -inf
	# 		# self.active_axis.config.I_bus_hard_max = inf
	# 		self.active_axis.controller.input_torque = value # TODO: was current
	# 		self.active_axis.controller.config.vel_limit = self.axis0Velocity_doubleSpinBox.value() # TODO: was current
	# 		# self.active_axis.controller.config.enable_torque_mode_vel_limit = False
	# 		print(self.active_axis.motor.effective_current_lim)
	# 	elif mode == ode.CONTROL_MODE_VELOCITY_CONTROL:
	# 		self.active_axis.controller.input_vel = value


def main():
	app = QtWidgets.QApplication(sys.argv)
	form = MainWindow()
	form.show()
	app.exec_()


if __name__ == '__main__':
	main()