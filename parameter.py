from PyQt5 import QtCore, QtGui, QtWidgets
from enum import Enum

from numpy import inf

class ParamTypes(Enum):
    Bool = 0
    ReadonlyString = 1
    Int = 2
    Float = 3
    Combobox = 4

class Parameter:
    def __init__(self, label, property_name, param_type, axis_specific, format_func=lambda x: x, write_processing_func=lambda x: x):
        self.property_name = property_name
        self.param_type = param_type
        self.axis_specific = axis_specific
        self.format_func = format_func
        self.write_processing_func = write_processing_func

        self.label_widget = QtWidgets.QLabel(label)
        if self.param_type is ParamTypes.ReadonlyString:
            self.field_widget = QtWidgets.QLabel("")
        elif self.param_type is ParamTypes.Bool:
            self.field_widget = QtWidgets.QCheckBox("")
        elif self.param_type is ParamTypes.Float:
            self.field_widget = QtWidgets.QDoubleSpinBox()
            self.field_widget.setMaximum(1E10)
            self.field_widget.setMinimum(-1E10)
        elif self.param_type is ParamTypes.Int:
            self.field_widget = QtWidgets.QSpinBox()
            self.field_widget.setMaximum(int(1E8))
            self.field_widget.setMinimum(int(-1E8))


    # def on_modify(self):
    #     self.mark_modified(self.read_value_from_ui() != self.last_read_value)

    def read(self, odrv_object, active_axis):
        """Read the value from the odrive to the UI element"""
        ob = active_axis if self.axis_specific else odrv_object
        for objname in self.property_name.split('.'):
            ob = getattr(ob, objname)
        value = self.format_func(ob)
        if self.param_type is ParamTypes.ReadonlyString:
            self.field_widget.setText(str(value))
        elif self.param_type is ParamTypes.Bool:
            self.field_widget.setChecked(value)
        elif self.param_type is ParamTypes.Float:
            self.field_widget.setValue(value)
        elif self.param_type is ParamTypes.Int:
            self.field_widget.setValue(value)

    def read_value_from_ui(self):
        value = None  # This function only needs to work for writable values
        if self.param_type is ParamTypes.Bool:
            return self.field_widget.isChecked()
        elif self.param_type in [ParamTypes.Float, ParamTypes.Int]:
            return self.field_widget.value()
        elif self.param_type is ParamTypes.Combobox:
            pass
            return self.combobox_mapping[self.ui_element.currentText()]

    def write(self, odrv_object, active_axis):
        """Write the value from the UI element into the odrive"""
        if self.param_type is ParamTypes.ReadonlyString:
            return
        if self.axis_specific:
            odrv_object = active_axis
        for objname in self.property_name.split('.')[:-1]:
            odrv_object = getattr(odrv_object, objname)
        value = self.write_processing_func(self.read_value_from_ui())  # TODO: maybe move write_processing_func to read_value_from_ui
        setattr(odrv_object, self.property_name.split('.')[-1], value)
        # self.last_read_value = self.read_value_from_ui()
        # self.on_modify()

        
    # def __init__(self, ui_element, property_name, datatype, writable, axis_specific, format_func=lambda x: x, write_processing_func=lambda x: x, combobox_mapping=None):
    #     """This class defines a parameter which should be read from the odrive and drawn on the screen. It may be read-only or writable
        
    #     datatype and writable defines how the element operates:
    #         string - read: Takes any data type and converts to string
    #         bool - read: Takes bool, sets pixmap to check or x
    #         bool_writable - read: Takes bool, and two UI elements in tuple. Sets one to be checked
    #         bool_writable - write: Writes checked state of first UI element from tuple
    #         num - read/write: uses value from UI element
    #         combobox - read/write: combobox_mapping should be {str: any} where the keys are the combobox entries, and the values are what should be read from/passed to the odrive
    #     format_func is applied to the data before it is drawn to the screen, and write_processing_func is applied to the data before it goes back to the odrive
    #     """
    #     self.ui_element = ui_element
    #     self.property_name = property_name
    #     self.datatype = datatype
    #     self.writable = writable
    #     self.axis_specific = axis_specific
    #     self.format_func = format_func
    #     self.write_processing_func = write_processing_func
    #     self.combobox_mapping = combobox_mapping
    #     if self.datatype == "combobox":
    #         self.ui_element.insertItems(0, self.combobox_mapping.keys())
    #     self.last_read_value = None
    #     if self.datatype == "num":
    #         self.ui_element.valueChanged.connect(self.on_modify)
    #     elif self.datatype == "bool_writable":
    #         self.ui_element[0].toggled.connect(self.on_modify)
    #     elif self.datatype == "combobox":
    #         self.ui_element.currentIndexChanged.connect(self.on_modify)

    # def mark_modified(self, is_modified):
    #     style = "background-color: rgb(246, 224, 143);" if is_modified else ""
    #     if self.datatype == 'bool_writable':
    #         for element in self.ui_element:
    #             element.setStyleSheet(style)
    #     else:
    #         self.ui_element.setStyleSheet(style)


    # def on_modify(self):
    #     self.mark_modified(self.read_value_from_ui() != self.last_read_value)

    # def read(self, odrv_object, active_axis):
    #     """Read the value from the odrive to the UI element"""
    #     ob = active_axis if self.axis_specific else odrv_object
    #     for objname in self.property_name.split('.'):
    #         ob = getattr(ob, objname)
    #     value = self.format_func(ob)
    #     if self.datatype == "string":
    #         self.ui_element.setText(str(value))
    #     elif self.datatype == "bool":
    #         self.ui_element.setPixmap(QtGui.QPixmap("Icons/True.jpg" if value else "Icons/False.jpg"))
    #     elif self.datatype == "bool_writable":
    #         self.ui_element[0 if value else 1].setChecked(True)
    #     elif self.datatype == "num":
    #         self.ui_element.setValue(value)
    #     elif self.datatype == "combobox":
    #         for k, v in self.combobox_mapping.items():
    #             if v == value:
    #                 self.ui_element.setCurrentText(k)
    #                 break
    #     self.last_read_value = self.read_value_from_ui()
    #     self.on_modify()

    # def write(self, odrv_object, active_axis):
    #     """Write the value from the UI element into the odrive"""
    #     if not self.writable:
    #         return
    #     if self.axis_specific:
    #         odrv_object = active_axis
    #     for objname in self.property_name.split('.')[:-1]:
    #         odrv_object = getattr(odrv_object, objname)
    #     value = self.write_processing_func(self.read_value_from_ui())
    #     setattr(odrv_object, self.property_name.split('.'